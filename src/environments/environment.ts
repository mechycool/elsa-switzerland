// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCZj2dnu-Jg65A-OE6FZ3EJomVOlBiO5Gg',
    authDomain: 'elsa-switzerland.firebaseapp.com',
    databaseURL: 'https://elsa-switzerland.firebaseio.com',
    projectId: 'elsa-switzerland',
    storageBucket: 'elsa-switzerland.appspot.com',
    messagingSenderId: '313131291137'
  }
};

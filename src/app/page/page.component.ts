import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Data} from '@angular/router';
import {About, GroupData, Imgs, Text} from '../resolvers/group-resolver.service';
import * as firebase from 'firebase';
import DocumentReference = firebase.firestore.DocumentReference;
import {Group} from '../resolvers/groups-resolver.service';
import {AngularFirestore} from 'angularfire2/firestore';
import {Project} from '../resolvers/projects-resolver.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  ref: DocumentReference;

  _title: string;
  set title(title: Text) {
    const lang = 'en'; // TODO implement TransalteService;
    this._title = title[lang];
    this.titleService.setTitle(`ELSA | ${title[lang]}`);
  }
  _coverImg: string;
  _boardImg: string;
  set imgs (imgs: Imgs) {
    this._coverImg = `url('${imgs.cover}')`;
    this._boardImg = imgs.board;
  }
  about: About;
  constructor(
    private route: ActivatedRoute,
    private titleService: Title,
    private db: AngularFirestore
  ) {}

  groups: Group[];
  news$: Promise<News[]>;
  allNews: boolean;
  projects: Project[];
  boardMembers$: Promise<BoardMember[]>;
  partners$: Promise<{
    strat: Partner[],
    acad: Partner[]
  }>;

  ngOnInit() {
    this.route.data.subscribe((data: {groupData: GroupData, groups: Group[], projects: Project[]}) => {
      this.ref = data.groupData.ref;
      this.title = data.groupData.title;
      this.about = data.groupData.about;
      this.imgs = data.groupData.imgs;
      this.groups = data.groups;
      this.getNews();
      this.projects = data.projects;
      this.getBoardMembers();
      this.getPartners();
    });
  }

  getNews() {
    this.news$ = this.ref.collection('news')
      .orderBy('date', 'desc')
      .get()
      .then(snap => {
        return snap.docs.map(doc => {
          return {
            ref: doc.ref,
            title: doc.data().title,
            date: doc.data().date,
            content: doc.data().content,
          };
        });
      });
  }
  getBoardMembers() {
    this.boardMembers$ = this.ref.collection('boardMembers')
      .orderBy('order')
      .get()
      .then(snap => {
        return snap.docs.map(doc => {
          return {
            ref: <DocumentReference> doc.ref,
            name: <string> doc.data().name,
            email: <string> doc.data().email,
            position: <Text> doc.data().position,
            img: <string> doc.data().img,
          };
        });
      });
  }

  change(ev) {
    console.clear();
    console.log(ev.html);
  }

  getPartners() {
    this.partners$ = this.ref.collection('partners')
      .get()
      .then(snap => {
        const partners = snap.docs.map(doc => {
          return {
            ref: <DocumentReference> doc.ref,
            type: <'strat'|'acad'> doc.data().type,
            content: <Text> doc.data().content,
            href: <string> doc.data().href,
            img: <string> doc.data().img,
          };
        });
        return {
          strat: partners.filter(partner => partner.type === 'strat'),
          acad: partners.filter(partner => partner.type === 'acad')
        };
      });
  }

}

interface News {
  ref: DocumentReference;
  title: Text;
  date: Date;
  content: Text;
}

interface BoardMember {
  ref: DocumentReference;
  name: string;
  position: Text;
  email: string;
  img: string;
}

interface Partner {
  ref: DocumentReference;
  type: 'strat'|'acad';
  img: string;
  href: string;
  content?: Text;
}

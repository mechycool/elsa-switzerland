import { BrowserModule } from '@angular/platform-browser';
import {NgModule, Provider} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { PageComponent } from './page/page.component';
import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {environment} from '../environments/environment';
import {GroupResolverService} from './resolvers/group-resolver.service';
import {GroupsResolverService} from './resolvers/groups-resolver.service';
import {ProjectsResolverService} from './resolvers/projects-resolver.service';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { ScrollSpyModule } from '@thisissoon/angular-scrollspy';
import {InViewportModule, WindowRef} from '@thisissoon/angular-inviewport';
import { QuillModule } from 'ngx-quill';
import {FormsModule} from '@angular/forms';

const getWindow = () => window;
const providers: Provider[] = [
  { provide: WindowRef, useFactory: (getWindow) },
];

@NgModule({
  declarations: [
    AppComponent,
    PageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    QuillModule,
    InViewportModule.forRoot(providers),
    ScrollSpyModule.forRoot(),
    ScrollToModule.forRoot()
  ],
  providers: [
    GroupResolverService,
    GroupsResolverService,
    ProjectsResolverService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PageComponent} from './page/page.component';
import {GroupResolverService} from './resolvers/group-resolver.service';
import {GroupsResolverService} from './resolvers/groups-resolver.service';
import {ProjectsResolverService} from './resolvers/projects-resolver.service';

const routeResolve = {
  groupData: GroupResolverService,
  groups: GroupsResolverService,
  projects: ProjectsResolverService
};

const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    resolve: routeResolve
  },
  {
    path: ':group',
    component: PageComponent,
    resolve: routeResolve
  },
  {
    path: '**',
    redirectTo: ''
  }
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { TestBed, inject } from '@angular/core/testing';

import { ProjectsResolverService } from './projects-resolver.service';

describe('ProjectsResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProjectsResolverService]
    });
  });

  it('should be created', inject([ProjectsResolverService], (service: ProjectsResolverService) => {
    expect(service).toBeTruthy();
  }));
});

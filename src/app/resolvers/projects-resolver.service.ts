import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import DocumentReference = firebase.firestore.DocumentReference;
import {Text} from './group-resolver.service';
import {Resolve} from '@angular/router';
import {AngularFirestore} from 'angularfire2/firestore';

@Injectable()
export class ProjectsResolverService implements Resolve<Project[]> {

  constructor(private db: AngularFirestore) {}

  resolve(): Promise<Project[]> {
    return this.db.collection('projects').ref.get()
      .then(snap => {
        return snap.docs.map(doc => {
          return {
            ref: doc.ref,
            title: <Text> doc.data().title,
            img: <string> doc.data().img,
            content: <Text> doc.data().content,
            href: <string> doc.data().href,
          };
        });
      });
  }
}

export interface Project {
  ref: DocumentReference;
  title: Text;
  img: string;
  content: Text;
  href: string;
}

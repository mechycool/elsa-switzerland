import { Injectable } from '@angular/core';
import {Resolve} from '@angular/router';
import {AngularFirestore} from 'angularfire2/firestore';
import {Text} from './group-resolver.service';

@Injectable()
export class GroupsResolverService implements Resolve<Group[]> {

  constructor(private db: AngularFirestore) {}
  resolve(): Promise<Group[]> {
    return this.db.collection('groups').ref.get()
      .then(snap => {
        return snap.docs
          .filter(doc => doc.id !== 'main')
          .map(doc => {
            return {
              title: <Text> doc.data().title,
              key: <string> doc.id
            };
          })
          .sort((a, b) => {
            if (a.key < b.key) return -1;
            if (a.key > b.key) return 1;
            return 0;
          });
      });
  }
}


export interface Group {
  title: Text;
  key: string;
}

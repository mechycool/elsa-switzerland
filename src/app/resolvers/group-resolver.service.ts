import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {AngularFirestore} from 'angularfire2/firestore';
import * as firebase from 'firebase';
import DocumentReference = firebase.firestore.DocumentReference;
import DocumentSnapshot = firebase.firestore.DocumentSnapshot;


@Injectable()
export class GroupResolverService implements Resolve<GroupData> {

  constructor(
    private db: AngularFirestore,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<GroupData> {
    const group = route.params.group || 'main';
    return this.db.collection('groups').doc(group).ref.get()
      .then(snap => {
        if (!snap.exists) {
          this.router.navigate(['']);
        }
        const data: GroupData = {
          ref: <DocumentReference> snap.ref,
          title: <Text> snap.data().title,
          vision: <string> '',
          about: <About> null,
          langs: <Langs> snap.data().langs,
          imgs: <Imgs> snap.data().imgs
        };
        return new Promise(resolve => {
          if (snap.id === 'main') {
            resolve(snap);
          } else {
            resolve(this.db.collection('groups').doc('main').ref.get());
          }
        })
          .then((mainSnap: DocumentSnapshot) => {
            data.vision = mainSnap.data().vision;
            data.about = {
              ...mainSnap.data().about,
              local: snap.data().info
            };
            return data;
          });
      });
  }
}


export interface GroupData {
  ref: DocumentReference;
  title: Text;
  vision: string;
  about: About;
  langs: Langs;
  imgs: Imgs;
}

export interface About {
  int: Text;
  swiss: Text;
  local: Text;
}

interface Langs {
  de: boolean;
  en: boolean;
  fr: boolean;
}
export interface Imgs {
  cover: string;
  board: string;
}

export interface Text {
  de?: string;
  en?: string;
  fr?: string;
}

import { TestBed, inject } from '@angular/core/testing';

import { BoardResolverService } from './board-resolver.service';

describe('BoardResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BoardResolverService]
    });
  });

  it('should be created', inject([BoardResolverService], (service: BoardResolverService) => {
    expect(service).toBeTruthy();
  }));
});

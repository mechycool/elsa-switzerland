import { TestBed, inject } from '@angular/core/testing';

import { GroupsResolverService } from './groups-resolver.service';

describe('GroupsResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GroupsResolverService]
    });
  });

  it('should be created', inject([GroupsResolverService], (service: GroupsResolverService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import {Text} from './group-resolver.service';

@Injectable()
export class BoardResolverService {
  constructor() {}
}


interface Member {
  name: string;
  img: string;
  position: Text;
  mail: string;
}
